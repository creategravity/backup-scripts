@Echo Off

rem created by terry@creategravity
rem 19:56 09/05/2017


rem REQUIRES THAT AN ARCHIVE CREATED BY SQLBACKUP IS DROPPED
rem ONTO THIS FILE, OR PASSED AS THE FIRST ARG
rem IT WILL TAKE CARE OF EVERYTHING ELSE


setlocal ENABLEDELAYEDEXPANSION

set DB=Database

set OUTPUTPATH=%cd%
set ARCFILENAME=%~1
set DBFILENAME=%ARCFILENAME:.7z=.bak%

rem extracts db from 7z archive to current folder
"C:\Program Files\7-Zip\7z" x "%ARCFILENAME%" -o%OUTPUTPATH%

SQLCMD -E -S .\SQLEXPRESS -Q "RESTORE DATABASE %DB% FROM DISK='%DBFILENAME%'"

del %DBFILENAME%
pause