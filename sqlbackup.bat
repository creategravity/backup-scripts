@Echo Off

rem created by terry@creategravity
rem 20:29 09/05/2017

setlocal ENABLEDELAYEDEXPANSION

set DB=Database

set LOGTIME=%TIME: =0%

rem names file as %DB%-YYYY-MM-DD_HH-MM.bak
set DBFILENAME=%DB%-%date:~6,4%-%date:~3,2%-%date:~0,2%_%LOGTIME:~0,2%-%LOGTIME:~3,2%

rem backs up db to current folder
SQLCMD -E -S .\SQLEXPRESS -Q "BACKUP DATABASE %DB% TO DISK='%cd%\%DBFILENAME%.bak'"

rem compresses into 7z format using fastest compression mode
"C:\Program Files\7-Zip\7z" a -mx=1 %DBFILENAME%.7z %DBFILENAME%.bak

rem removes original db file
del %DBFILENAME%.bak


rem generate the ftp script. will overwrite any existing script
echo open ftp.creategravity.com> temp.ftpcmd
echo username>> temp.ftpcmd
echo password>> temp.ftpcmd
echo binary>> temp.ftpcmd
echo lcd %cd%
echo put %DBFILENAME%.7z>> temp.ftpcmd
echo quit>> temp.ftpcmd

rem launch ftp and pass it the script
ftp -s:temp.ftpcmd

rem clean up.
del temp.ftpcmd