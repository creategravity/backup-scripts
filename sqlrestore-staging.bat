@Echo Off

rem created by terry@creategravity
rem 19:56 09/05/2017

setlocal ENABLEDELAYEDEXPANSION

set DB=Database
set DBDEST=DatabaseAlt

set OUTPUTPATH=%cd%
set ARCFILENAME=%~1
set DBFILENAME=%ARCFILENAME:.7z=.bak%

rem extracts db from 7z archive to current folder
"C:\Program Files\7-Zip\7z" x "%ARCFILENAME%" -o%OUTPUTPATH%

rem paths will have to change to match your sql server paths
SQLCMD -E -S .\SQLEXPRESS -Q "RESTORE DATABASE %DBDEST% FROM DISK='%DBFILENAME%' WITH MOVE '%DB%_Data' TO 'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\%DBDEST%_Data.mdf', MOVE '%DB%_Log' TO 'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\%DBDEST%_Log.ldf'"

del %DBFILENAME%
pause