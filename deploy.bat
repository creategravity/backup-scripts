@Echo Off


rem created by terry@creategravity
.com
rem 13:54 20/09/2016


rem stop the windows service
net stop WebServices

rem stops application pool, allows dlls to be overwritten
%systemroot%\system32\inetsrv\appcmd stop apppool /apppool.name:"Website Name"

robocopy C:\Jenkins\workspace\Services\bin\Release\ C:\inetpub\domains\example.com\Services\ /E /XF *.config *.cs *.csproj* *.log

robocopy C:\Jenkins\workspace\Web\ C:\inetpub\domains\example.com\Web /E /XF *.config *.cs *.log


rem /E  : Copy Subfolders, including Empty Subfolders.

rem /XO : eXclude Older - if destination file exists and is the same date or newer than the source - don't overwrite it.
rem /XF : excludes all *.config *.cs *.csproj* *.log files from copy operation

net start WebServices

rem starts application after the folder has been migrated, allows dlls to be loaded
%systemroot%\system32\inetsrv\appcmd start apppool /apppool.name:"Website Name"

echo dramatic pause
timeout 5 

echo all done.
